package kr.amc.cloud.amis.prologue.type;

public enum MedexCode {
    //
    EMERGENCY("E"),
    OUTPATIENT("O")
    ;

    private String code;
    MedexCode(String code){
        //
        this.code = code;
    }

    public String getCode() {
        //
        return code;
    }
}
