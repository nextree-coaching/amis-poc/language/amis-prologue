package kr.amc.cloud.amis.prologue.granule;

import io.naraway.accent.domain.ddd.ValueObject;
import io.naraway.accent.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Address implements ValueObject {
    //
    private String zipCode;
    private String zipAddress;
    private String streetAddress;

    public String toString() {
        //
        return toJson();
    }

    public static Address fromJson(String json) {
        //
        return JsonUtil.fromJson(json, Address.class);
    }

    public static Address sample() {
        //
        return new Address("05505",
                "서울특별시 송파구 올림픽로 43길 88 서울아산병원",
                "생명과학연구원 6F 601호");
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }

}
