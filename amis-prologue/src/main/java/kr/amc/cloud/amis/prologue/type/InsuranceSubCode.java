package kr.amc.cloud.amis.prologue.type;

public enum InsuranceSubCode {
    //
    GENERAL("A"),       // 일반
    SELF("C"),          // 자부담 100
    SEVERELY("E"),      // 중증
    RARE_DISEASE("B")   // 희귀난치
    ;

    private String code;
    InsuranceSubCode(String code){
        //
        this.code = code;
    }

    public String getCode() {
        //
        return code;
    }
}
