package kr.amc.cloud.amis.prologue.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/envoy/healthCheck")
public class HealthCheck {
    //
    @GetMapping
    public String hello() {
        //
        return "OK";
    }
}
