package kr.amc.cloud.amis.prologue.type;

public enum InsuranceCode {
    //
    HEALTH_INSURANCE("10"), // 건강보험
    GENERAL("41"),          // 일반보험
    AUTOMOBILE("42")        // 자동차보험
    ;

    private String code;
    InsuranceCode(String code){
        //
        this.code = code;
    }

    public String getCode() {
        //
        return code;
    }
}
