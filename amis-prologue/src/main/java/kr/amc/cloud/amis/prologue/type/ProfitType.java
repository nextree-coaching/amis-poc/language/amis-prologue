package kr.amc.cloud.amis.prologue.type;

public enum ProfitType {
    //
    OUTPATIENT("O"),
    INPATIENT("I")
    ;
    private String code;
    ProfitType(String code){
        //
        this.code = code;
    }

    public String getCode() {
        //
        return code;
    }
}
